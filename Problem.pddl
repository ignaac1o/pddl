(define (problem prob)
 (:domain pastelero)
 (:objects
    leche harina azucar huevo levadura vainilla canela aceite yema clara 
    clara_montada zanahoria mantequilla galleta nata cuajada yogur 
    chocolate gelatina 
    galleta_mantequilla
    galleta_triturada
    bleche bgenoves bzanahoria tartachoco tartayogur - alimento
    izq der - hand
    batidora horno nevera hornillo
    varilla espatula - utensilio
    bol1 bol2 bol3 bol4 bolm 
    olla olla2 - contenedor
    molde1 molde2 molde3 - molde
)

 (:init
    (ontable leche)
    (ontable harina)
    (ontable azucar)
    (ontable huevo)
    (ontable levadura)
    (ontable vainilla)
    (ontable canela)
    (ontable aceite)
    (ontable zanahoria)
    (ontable mantequilla)
    (ontable galleta)
    (ontable chocolate)
    (ontable gelatina)
    (ontable nata)
    (ontable yogur)
    (ontable cuajada)

   (ontable varilla)
   (ontable espatula)
   (ontable bol1)
   (ontable bol2)
   (ontable bol3)
   (ontable bol4)
   (ontable bolm)
   (ontable olla)
   (ontable olla2)
   (ontable molde1)
   (ontable molde2)
   (ontable molde3)


   (handempty izq)
   (handempty der)

   (empty batidora)
   (empty horno)
   (empty hornillo)
   (empty nevera)

   (empty molde1)
   (empty molde2)
   (empty molde3)

   (clean bol1)
   (clean bol2)
   (clean bol3)
   (clean bol4)
   (clean bolm)
   (clean olla)
   (clean olla2)
   (clean molde1)
   (clean molde2)
   (clean molde3)


;MEZCLAS COMUNES
(untar mantequilla)


;BIZCOCHO DE LECHE (BLECHE)
    
   (part-of azucar bleche)
   (part-of huevo bleche)
   (part-of harina bleche)
   (part-of levadura bleche)
   (part-of aceite bleche)
   (part-of leche bleche)
   (part-of canela bleche)
   (part-of vainilla bleche)

   (add_order harina levadura bleche)
   (add_order azucar huevo bleche)

   (requires bol1 bleche)
   (requires batidora bleche)
   (cookedon horno bleche)

;BIZCOCHO GENOVES (BGENOVES)

   (dividimos huevo clara yema)
   (montamos varilla clara clara_montada)

   (part-of azucar bgenoves)
   (part-of yema bgenoves)
   (part-of clara_montada bgenoves)
   (part-of harina bgenoves)

   (add_order azucar yema bgenoves)
   (add_order yema clara_montada bgenoves)


   (requires bol2 bgenoves)
   (requires batidora bgenoves)
   (cookedon horno bgenoves)
   

;BIZCOCHO ZANAHORIA (BZANAHORIA)

   (part-of huevo bzanahoria)
   (part-of azucar bzanahoria)
   (part-of aceite bzanahoria)
   (part-of harina bzanahoria)
   (part-of levadura bzanahoria)
   (part-of zanahoria bzanahoria)

   (add_order harina levadura bzanahoria)
   (add_order azucar huevo bzanahoria)
   (add_order huevo zanahoria bzanahoria)

   (requires bol3 bzanahoria)
   (requires batidora bzanahoria)
   (cookedon horno bzanahoria)

;MEZCLAS BASE TARTAS

   (part-of galleta_triturada galleta_mantequilla)
   (part-of mantequilla galleta_mantequilla)
   (add_order galleta_triturada mantequilla galleta_mantequilla)
   (need varilla galleta_mantequilla)
   (requires bolm galleta_mantequilla)
   (use espatula galleta_mantequilla)
   (triturar galleta galleta_triturada)

;TARTA DE CHOCOLATE
   (part-of leche tartachoco)
   (part-of chocolate tartachoco)
   (part-of gelatina tartachoco)
   (part-of azucar tartachoco)
   (part-of nata tartachoco)

   (add_order azucar nata tartachoco)

   (requires olla tartachoco)
   (need varilla tartachoco)
   (melt hornillo tartachoco)
   (tarta tartachoco)
   (cookedon nevera tartachoco)

;TARTA DE YOGUR
   (part-of leche tartayogur)
   (part-of yogur tartayogur)
   (part-of cuajada tartayogur)
   (part-of azucar tartayogur)
   (part-of nata tartayogur)

   (add_order azucar nata tartayogur)

   (requires olla2 tartayogur)
   (need varilla tartayogur)
   (melt hornillo tartayogur)
   (tarta tartayogur)
   (cookedon nevera tartayogur)
 )

 (:goal (and
   ;(ontable tartayogur)
   (ontable tartachoco)
   ;(ontable bgenoves)
   ;(ontable bleche)
   ;(ontable bzanahoria)
   )
 )

)
