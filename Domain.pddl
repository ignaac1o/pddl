(define (domain pastelero)
 (:requirements :adl)
 (:types
      alimento recurso hand - objeto
      contenedor  utensilio  - recurso 
      molde - contenedor
 )
 (:predicates

    ;PREDICADOS BASICOS DEL PROBLEMA
    (ontable ?obj - objeto)
    (holding ?h - hand ?obj - objeto)
    (handempty ?h - hand)
    (clean ?rec - recurso)
    (empty ?rec - recurso)
    (contains ?rec - contenedor ?al - alimento)

    ;PREDICADOS ACCIONES DIVIDIR INGREDIENTE EN DOS
    (dividimos ?al1 ?al2 ?al3 - alimento)
    (montamos ?utensilio - utensilio ?al1 ?mez - alimento)

    ;PREDICADOS ACCION MEZCLAS INGREDIENTES
    (part-of ?part ?mezcla - alimento)
    (requires ?rec - recurso ?mezcla - alimento)
    (complete ?al - alimento)
    (add_order ?part1 ?part2 ?mezcla - alimento)
    (incorporado ?part ?mezcla - alimento)

    ;PREDICADOS ESPECIFICOS BIZCOCHOS
    (onshaker ?utensilio - utensilio ?mezcla - alimento)
    (shaked ?mezcla - alimento)
    (heating-oven)
    (untar ?alimento - alimento)

    ;PREDICADOS COCINAR BIZCOHOS Y TARTAS
    (ready_molde ?molde - contenedor)
    (cookedon ?utensilio - utensilio ?mezcla - alimento)
    (onrecurso ?utensilio - utensilio ?mezcla - alimento)
    (cooked ?mezcla - alimento)
    (base_mantequilla_lista ?molde - contenedor)

 
    ;PREDICADOS ESPECIFICOS TARTAS
    (need ?varilla - utensilio ?mezcla - alimento )
    (mezclado ?alimento - alimento)
    (use ?utensilio - utensilio ?alimento - alimento)
    (tarta ?alimento - alimento)
    (base_galleta_lista ?molde - contenedor)
    (onhornillo ?mezcla - alimento)
    (heated ?mezcla - alimento)
    (melt ?utensilio - utensilio ?mezcla - alimento)
    (heating-hornillo)
    (triturar ?alimento ?alimento2 - alimento)
 )
 
 (:action agarrar
    :parameters (?h - hand ?obj - objeto)
    :precondition (and
                     (ontable ?obj)
                     (handempty ?h)
                     (not (holding ?h ?obj))
                  )
    :effect (and
            (not(handempty ?h))
            (holding ?h ?obj)
            (not(ontable ?obj))
            )
 )
 
 (:action soltar
    :parameters (?h - hand ?obj - objeto)
    :precondition (and
                    (holding ?h ?obj)
                    (not(handempty ?h))
                  ) 
    :effect (and
            (ontable ?obj)
            (handempty ?h)
            (not (holding ?h ?obj))
            )
 )


 (:action hacer_mezcla
   :parameters (?h - hand ?bol - contenedor ?part ?mezcla - alimento)
   :precondition (and
         (holding ?h ?part)
         (ontable ?bol)
         (part-of ?part ?mezcla)
         (requires ?bol ?mezcla)

         (forall (?prev - alimento)
            (imply(add_order ?prev ?part ?mezcla)
                  (incorporado ?prev ?mezcla)))

   )
   :effect (and
         (incorporado ?part ?mezcla)
         (contains ?bol ?part)

         (when (and (not (exists (?p - alimento)
                     (and (part-of ?p ?mezcla)
                          (not (= ?p ?part))
                     (not (incorporado ?p ?mezcla)))))
                  )
            (and (complete ?mezcla)
                 (contains ?bol ?mezcla)
                 (not(shaked ?mezcla))))
      )
 )

(:action separar_huevo_en_yema_clara
    :parameters (?h1 ?h2 - hand ?al1 ?al2 ?al3 - alimento)
    :precondition (and
            (holding ?h1 ?al1)
            (not(handempty ?h1))
            (handempty ?h2)
            (dividimos ?al1 ?al2 ?al3)
            )
    :effect (and
            (ontable ?al1)
            (not(handempty ?h1))
            (not(handempty ?h2))
            (not (holding ?h1 ?al1))
            (not (ontable ?al2))
            (not (ontable ?al3))
            (holding ?h1 ?al2)
            (holding ?h2 ?al3)
            )
 )

(:action montar
     :parameters (?ing - alimento ?h1 - hand ?var - utensilio  ?mez - alimento )
     :precondition (and
            (holding ?h1 ?var)
            (not (handempty ?h1))
            (ontable ?ing)
            (montamos ?var ?ing ?mez)
     )
     :effect (and
            (ontable ?mez)
            (not(ontable ?ing))
     )
 )

 (:action clean
    :parameters(?h1 - hand ?rec - contenedor)
    :precondition(and
            (holding ?h1 ?rec)
            (not (handempty ?h1))
            (empty ?rec)
            (not(clean ?rec))
    )
    :effect(and
        (clean ?rec)
        (not(ready_molde ?rec))
        ;(not(base_mantequilla_lista ?rec))
        ;(not(base_galleta_lista ?rec))
    )
 )

 (:action meter_en_batidora
    :parameters(?h - hand ?bol - contenedor ?bat - utensilio ?mezcla - alimento)
    :precondition(and
                (contains ?bol ?mezcla)
                (requires ?bol ?mezcla)
                (holding ?h ?bol)
                (not (handempty ?h))
                (requires ?bat ?mezcla)
                (not(shaked ?mezcla))
                (empty ?bat)
    )
    :effect(and 
            (not(holding ?h ?bol))
            (handempty ?h)
            (not (empty ?bat))
            (onshaker ?bat ?mezcla)
    )
 )

 (:action batir
   :parameters (?bol - contenedor ?bat - utensilio ?mezcla - alimento)
   :precondition (and
            (contains ?bol ?mezcla)
            (requires ?bol ?mezcla)
            (onshaker ?bat ?mezcla)
            (not(shaked ?mezcla))
            (not(empty ?bat)) 
   )
   :effect (and 
            (shaked ?mezcla)
            (not (requires ?bat ?mezcla))
   )
 )

 (:action sacar_de_batidora
    :parameters(?h - hand ?bol - contenedor ?bat - utensilio ?mezcla - alimento)
    :precondition(and
                (onshaker ?bat ?mezcla)
                (handempty ?h)
                (not (empty ?bat))
                (shaked ?mezcla)
                (contains ?bol ?mezcla)
                (requires ?bol ?mezcla)
    )
    :effect(and
            (holding ?h ?bol)
            (not (handempty ?h))
            (empty ?bat)
            (not (onshaker ?bat ?mezcla))
    )
 )

 (:action preparar_molde
     :parameters (?h1 - hand ?mantequilla - alimento ?molde - molde)
     :precondition (and
               (ontable ?molde)
               (holding ?h1 ?mantequilla)
               (not(handempty ?h1))
               (empty ?molde)
               (clean ?molde) 
               (untar ?mantequilla)
               (not (base_mantequilla_lista ?molde))
               (not (base_galleta_lista ?molde))
     )
     :effect (and
            (not(clean ?molde))
            (base_mantequilla_lista ?molde) 
     )
 )

 (:action llenar_molde
    :parameters(?h - hand ?bol - contenedor ?molde - molde ?mezcla - alimento)
    :precondition (and
                    (holding ?h ?bol)
                    (not (handempty ?h))
                    (contains ?bol ?mezcla)
                    (requires ?bol ?mezcla)
                    (empty ?molde)
                    (ontable ?molde)
                    (not (clean ?molde))
                    (base_mantequilla_lista ?molde)
                (or
                        (and(shaked ?mezcla)(not (cooked ?mezcla)))
                        (and(heated ?mezcla)(base_galleta_lista ?molde))
                )
                        (not(ready_molde ?molde))
        )     
    :effect(and
            (empty ?bol)
            (not (empty ?molde))
            (not (clean ?bol))
            (not (contains ?bol ?mezcla))
            (contains ?molde ?mezcla)
            (ready_molde ?molde)
    )
)

(:action meter_en
    :parameters (?recurso - utensilio ?h1 - hand ?molde - molde ?mezcla - alimento)
    :precondition (and 
            (holding ?h1 ?molde)
            (not (handempty ?h1))
            (contains ?molde ?mezcla)
            (base_mantequilla_lista ?molde)
            
            (or (and(shaked ?mezcla)(heating-oven))
                (and(heated ?mezcla)(base_galleta_lista ?molde))
            )
            
            (ready_molde ?molde)

            (not (cooked ?mezcla))
            (not (onrecurso ?recurso ?mezcla))
            (empty ?recurso)
            (cookedon ?recurso ?mezcla)
   )
    :effect (and 
            (not (holding ?h1 ?molde))
            (handempty ?h1)
            (onrecurso ?recurso ?mezcla)
            (not (empty ?recurso))
    )
)

(:action cocinandose_en
   :parameters (?recurso - utensilio ?molde - molde  ?mezcla - alimento)
   :precondition (and
            (onrecurso ?recurso ?mezcla)
            (contains ?molde ?mezcla)
            (not(empty ?recurso)) 
   )
   :effect (and 
            (cooked ?mezcla)
   )
)

(:action sacar_de
    :parameters (?recurso - utensilio ?h1 - hand ?molde - molde  ?mezcla - alimento)
    :precondition (and
               (handempty ?h1)
               (cooked ?mezcla)
               (contains ?molde ?mezcla)
               (onrecurso ?recurso ?mezcla)
               (not (empty ?recurso)) 
    )
    :effect (and 
            (not (onrecurso ?recurso ?mezcla))
            (empty ?recurso)

            (when (not(tarta ?mezcla))
                (not(heating-oven))
            )

            (ontable ?mezcla)
            (ontable ?molde)
            (not (contains ?molde ?mezcla))
            (empty ?molde)
            (not (clean ?molde))
            (not(base_galleta_lista ?molde))
            (not(base_mantequilla_lista ?molde))
            (not(ready_molde ?molde))
    )
)

(:action calentar_horno
    :parameters ()
    :precondition (not(heating-oven))
    :effect (heating-oven)
)

(:action encender_hornillo
    :parameters ()
    :precondition (not(heating-hornillo))
    :effect (heating-hornillo)
)

(:action mezclar
    :parameters (?h - hand ?contenedor - contenedor ?varilla - utensilio ?mezcla - alimento)
    :precondition (and 
               (contains ?contenedor ?mezcla)
               (requires ?contenedor ?mezcla)
               (or      
                        (not(tarta ?mezcla))
                        (and(onhornillo ?mezcla)(tarta ?mezcla))
                )
               (not(handempty ?h))
               (holding ?h ?varilla)
               (need ?varilla ?mezcla)
               (not (mezclado ?mezcla))
    )
    :effect   (mezclado ?mezcla)
)

(:action preparar_molde_galleta
        :parameters (?h2 - hand ?espatula - utensilio ?h1 - hand ?bol - contenedor ?molde - molde  ?galleta - alimento)
        :precondition (and
                (use ?espatula ?galleta)
                (holding ?h1 ?bol)
                (holding ?h2 ?espatula)
                (not (handempty ?h1))
                (not (handempty ?h2))
                (contains ?bol ?galleta)
                (mezclado ?galleta)
                (empty ?molde)
                (not(clean ?molde))
                (ontable ?molde)
                (base_mantequilla_lista ?molde)
                (not(base_galleta_lista ?molde))
        )
        :effect (and 
                (base_galleta_lista ?molde)
        )
)

 (:action poner_en_hornillo
    :parameters(?h - hand ?bol - contenedor ?hornillo - utensilio ?mezcla - alimento)
    :precondition(and
                (contains ?bol ?mezcla)
                (requires ?bol ?mezcla)
                (complete ?mezcla)
                (holding ?h ?bol)
                (not (handempty ?h))
                (melt ?hornillo ?mezcla)
                (not(mezclado ?mezcla))
                (empty ?hornillo)
                (heating-hornillo)
                (not(heated ?mezcla))
    )
    :effect(and 
            (not(holding ?h ?bol))
            (handempty ?h)
            (not (empty ?hornillo))
            (onhornillo ?mezcla)
    )
 )

  (:action quitar_de_hornillo
    :parameters(?h - hand ?bol - contenedor ?hornillo - utensilio ?mezcla - alimento)
    :precondition(and
                (onhornillo ?mezcla)
                (handempty ?h)
                (not (empty ?hornillo))
                (mezclado ?mezcla)
                (contains ?bol ?mezcla)
                (requires ?bol ?mezcla)
                (heating-hornillo)
                (melt ?hornillo ?mezcla)
    )
    :effect(and
            (holding ?h ?bol)
            (not (handempty ?h))
            (empty ?hornillo)
            (not (onhornillo ?mezcla))
            (not(heating-hornillo))
            (heated ?mezcla)
    )
 )

 (:action triturar
   :parameters (?h - hand ?ingrediente ?triturado - alimento)
   :precondition (and 
                (holding ?h ?ingrediente)
                (not(handempty ?h))
                (triturar ?ingrediente ?triturado)
   )
   :effect (and 
                (not(holding ?h ?ingrediente))
                (ontable ?ingrediente)
                (holding ?h ?triturado)
                (not(handempty ?h))
   )
)

)